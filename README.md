## 欧洲澳洲英国爱尔兰新西兰等国家计算机专业申请汇集

1. 这里的专业全部都是英语授课
2. 全部都是计算机类 (CS 和 EE)
3. 全部都是研究生专业
4. 生活费只是估计值，因为一线城市和二线城市租房价格差距较大，生活成本也会比较大。

国家 | 时间 |  学费 | 生活费 
---|---|---|---
爱尔兰 | 1 年或者 2 年 |  2.3w 欧左右 | 1.2 w欧 /年
挪威 | 1 年或者 2 年 | 免学费 | 1.5 w欧 /年
瑞典 | 1 年或者 2 年 | 1.8w 欧左右 | 1 w欧 /年
芬兰 | 1 年或者 2 年 | 1.8w 欧左右 | 1 w欧 /年
丹麦 | 1 年或者 2 年 | 1.8w 欧左右 | 1 w欧 /年
荷兰 | 1 年或者 2 年 | 1.8w 欧左右 | 1 w欧 /年
德国 | 1 年或 1.5 年或 2 年 | 免学费或者 5.5K 欧左右 | 1 w欧 /年
西班牙 | 1 年或 1.5 年或 2 年 | 4K-5.5K 欧左右 | 1 w欧 /年
意大利 | 1.5 年或 2 年 | 5K 欧左右 |  1 w欧 /年
